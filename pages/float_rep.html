


<!DOCTYPE html>

<html lang="English">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.18.1: http://docutils.sourceforge.net/" />

    <title>12. Be careful: Working with floats &#8212; AIMS Python 0.7 documentation</title>
    <link rel="stylesheet" type="text/css" href="../_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="../_static/cloud.css" />
    <link rel="stylesheet" type="text/css" href="../_static/style.css" />
    <link rel="stylesheet" type="text/css" href="../_static/table_styling.css" />
    <link rel="stylesheet" href="../local_PT_sans" type="text/css" />
    
    <script data-url_root="../" id="documentation_options" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/_sphinx_javascript_frameworks_compat.js"></script>
    <script src="../_static/doctools.js"></script>

    
    
     
        <script src="../_static/jquery.cookie.js"></script>
    

    
     
        <script src="../_static/cloud.base.js"></script>
    

    
     
        <script src="../_static/cloud.js"></script>
    

    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="13. Conditionals, I: if, else and more" href="conditionals.html" />
    <link rel="prev" title="11. Bools and Boolean expressions" href="boolean_expr.html" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
  </head><body>
    <div class="relbar-top">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="conditionals.html" title="13. Conditionals, I: if, else and more"
             accesskey="N">next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="boolean_expr.html" title="11. Bools and Boolean expressions"
             accesskey="P">previous</a> &nbsp; &nbsp;</li>
    <li><a href="../index.html">AIMS Python 0.7 documentation</a> &#187;</li>

        <li class="nav-item nav-item-this"><a href=""><span class="section-number">12.  </span>Be careful: Working with floats</a></li> 
      </ul>
    </div>
    </div>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="be-careful-working-with-floats">
<span id="float-rep"></span><h1><span class="section-number">12.  </span>Be careful: Working with floats<a class="headerlink" href="#be-careful-working-with-floats" title="Permalink to this heading">¶</a></h1>
<nav class="contents local" id="contents">
<ul class="simple">
<li><p><a class="reference internal" href="#binary-vs-decimal-representation-of-floats" id="id1">Binary vs decimal representation of floats</a></p></li>
<li><p><a class="reference internal" href="#consequences-of-binary-approximation" id="id2">Consequences of binary approximation</a></p></li>
<li><p><a class="reference internal" href="#practice" id="id3">Practice</a></p></li>
</ul>
</nav>
<p><a class="reference internal" href="basic_ops_types.html#basic-ops-exact"><span class="std std-ref">In a previous section</span></a>, we saw that there
were several reasons that we should <em>not</em> expect floating point
evaluations and representations to be exact.  There were issues of
having finite degrees of decimal precision, rounding and truncation.
For example, the result of evaluating <code class="docutils literal notranslate"><span class="pre">2**0.5</span></code> can only ever be an
approximation to the true mathematical value, which has an infinite
number of decimal places; the Python result of <code class="docutils literal notranslate"><span class="pre">1.4142135623730951</span></code>
is pretty detailed and generally a useful approximation, but it is not
exact.</p>
<p>Those issues were essentially observable when results had a lot of
decimal places to represent, and approximations were needed.  But what
about evaluations of floating point numbers that only require one or
two decimal places, like 0.1, 0.25, -8.5, etc.: <em>they look safe to
treat as exact, but are they?</em></p>
<section id="binary-vs-decimal-representation-of-floats">
<span id="float-rep-bin"></span><h2><a class="toc-backref" href="#id1" role="doc-backlink"><span class="section-number">12.1.  </span>Binary vs decimal representation of floats</a><a class="headerlink" href="#binary-vs-decimal-representation-of-floats" title="Permalink to this heading">¶</a></h2>
<p>As humans, we typically work with and write numbers in base-10 (like
we did just up above), which is literally what &quot;decimal&quot; means.
However, the underlying code interpreters work in base-2, or what is
called operating in <strong>binary</strong>.  The base-10 quantities we type into
the Python environment are translated into &quot;machine speak&quot;, and the
actual calculations are done in binary using <strong>bits</strong> (which only take
values 1 and 0) and <strong>bytes</strong> (a basic grouping of eight bits).  So
let's look at floating point values in each representation.</p>
<p>In general, to finitely represent a number in a particular base <em>B</em>,
we have to be able to write it as a fraction where:</p>
<ul>
<li><div class="line-block">
<div class="line">the numerator is a finite sum of integer coefficients times powers
of <em>B</em>, i.e.:</div>
<div class="line"><img class="math" src="../_images/math/681d8cc9279a230bee8f908b07723a20aeb5c2d7.svg" alt="x_0 \times B^0 + x_1 \times B^1 + x_2 \times
B^2 + \dots+ x_M \times B^M"/>, for finite <em>M</em>.</div>
</div>
</li>
<li><p>the denominator is a single integer power of <em>B</em>, i.e.: <img class="math" src="../_images/math/565c2ee462218519ca4ca2b79e7a84728839ecd3.svg" alt="B^N"/>,
for finite <em>N</em>.</p></li>
</ul>
<p><em>Sidenote:</em> The numerator's coefficients represent the digits of a
number. So, in base-10, <img class="math" src="../_images/math/025143c2ec9495f1b982198428ae12b9ed213be1.svg" alt="x_0"/> is the ones digit, <img class="math" src="../_images/math/0a3d6cb24ae0840f5741cedcfa8f1dd79b5c38d2.svg" alt="x_1"/> the
tens, etc. Therefore, the sums in any base are often written in order
of decreasing exponent, so the coefficient order matches how they slot
into the number they represent.</p>
<p><strong>Case A.</strong> Consider 7.0, which we can express as the fraction
<img class="math" src="../_images/math/b463d712620e5846856f2df7451a82ac5c8f0ce2.svg" alt="7/1"/>.  In base-10, the numerator would be just the single term
<img class="math" src="../_images/math/8ee411c61e7fb2f189b7db3632e1f26eda308d80.svg" alt="7\times10^0"/>, and the denominator is made unity by raising the
base to zero, <img class="math" src="../_images/math/9f775cd8d35e31eae14fbad6ac84d5fbb4514611.svg" alt="10^0"/>.  In base-2, the numerator has more
components <img class="math" src="../_images/math/2907511885adafbb9ab220f505f6cc3434877876.svg" alt="1\times2^2+1\times2^1+1\times2^0"/>, but the
denominator is made unity in the same way, <img class="math" src="../_images/math/d20772419ee80dbc8b532f5ed78e3c38d9d7473a.svg" alt="2^0"/>.  So, we have
done the job of showing that a finite, &quot;exact&quot; representation is
possible for this number in either base-10 or base-2.</p>
<p><strong>Case B.</strong> Consider 0.5.  In decimal representation of a fraction, we
have to find an integer that is a sum of powers of ten, that can be
divided by some power of 10 to provide our value of interest.  This is
solvable with a finite number of terms in the numerator, basically
reading from the decimal representation:</p>
<p><img class="math" src="../_images/math/c97c50ef2602fdd2afc18af6d48fbdd9239a8d83.svg" alt="0.5 = \dfrac{5}{10} = \dfrac{5 \times 10^{0}}{10^1}\,
\rightarrow{\rm ~job~done}."/></p>
<p>Thus, this number has a finite decimal representation, as expected.
In terms of a binary representation, a similar rule applies with
powers of 2, which is also solvable (by looking at the fraction
notation:</p>
<p><img class="math" src="../_images/math/b8b337472dbef5700ed225c368ed03f186e423fc.svg" alt="0.5 = \dfrac{5}{10} = \dfrac{1}{2} = \dfrac{1 \times
2^{0}}{2^1}\, \rightarrow{\rm ~job~done}."/></p>
<p>So, both the decimal and binary representations are finite for this
number.</p>
<p><strong>Case C.</strong> Now consider the humble <code class="docutils literal notranslate"><span class="pre">0.1</span></code>, which initially looks to
be a repetition of Case B, above.  First, the decimal representation
is:</p>
<p><img class="math" src="../_images/math/799a7b9447e81ee0f1bfc7a7f94aef3045060b2b.svg" alt="0.1 = \dfrac{1}{10} = \dfrac{1 \times 10^{0}}{10^1}\,
\rightarrow{\rm ~job~done}."/></p>
<p>However, in binary, we have a problem finding a denominator that will
fit with any representation we try---they never seem to be an integer
power of 2:</p>
<p><img class="math" src="../_images/math/e2543865ac1c2835c9a4032182e0e2ca6357533b.svg" alt="0.1 = \dfrac{1}{10} \neq \dfrac{1 \times 2^{0}}{2^3} = 0.125\,
\rightarrow{\rm ~try~again},"/></p>
<p><img class="math" src="../_images/math/ea3ef1ba157ea4ac5d65b58572fe47658fe185e0.svg" alt="0.1 = \dfrac{2}{20} \neq \dfrac{1 \times 2^{1}}{2^4} = 0.125\,
\rightarrow{\rm ~try~again},"/></p>
<p><img class="math" src="../_images/math/3b22976ebcffcf4822e16b97ea5c5f8ce886be37.svg" alt="0.1 = \dfrac{3}{30} \neq \dfrac{1 \times 2^{1} + 1 \times
2^{0}}{2^5} = 0.09375\, \rightarrow{\rm ~try~again},"/></p>
<p><img class="math" src="../_images/math/7eab0743517d17716ad3a383b0a1f244e5a17b40.svg" alt="\dots"/></p>
<p>It turns out that we can <em>never</em> find a satisfactory denominator, and
there is <em>no</em> exact, finite representation of 0.1 in
binary. Therefore, Python internally uses just a fractional
approximation (to be precise, <code class="docutils literal notranslate"><span class="pre">3602879701896397</span> <span class="pre">/</span> <span class="pre">2**55</span></code>). Thus,
<strong>computers can introduce rounding or truncation error even when
representing finite decimal numbers.</strong> We can only approximate 0.1
(and other decimals with similar properties) in Python.</p>
</section>
<section id="consequences-of-binary-approximation">
<h2><a class="toc-backref" href="#id2" role="doc-backlink"><span class="section-number">12.2.  </span>Consequences of binary approximation</a><a class="headerlink" href="#consequences-of-binary-approximation" title="Permalink to this heading">¶</a></h2>
<p>We can see the effects of the unavoidable, internal binary
approximations with a few examples.</p>
<p>First, note that the expression <code class="docutils literal notranslate"><span class="pre">5.1</span> <span class="pre">%</span> <span class="pre">1</span></code> evaluates to
<code class="docutils literal notranslate"><span class="pre">0.09999999999999964</span></code>, instead of to <code class="docutils literal notranslate"><span class="pre">0.1</span></code>.  And we might have
expected each of the following to evaluate to <code class="docutils literal notranslate"><span class="pre">True</span></code>, but not all
do:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="mf">5.5</span> <span class="o">%</span> <span class="mi">1</span> <span class="o">==</span> <span class="mf">0.5</span>            <span class="c1"># True</span>
<span class="mf">5.4</span> <span class="o">%</span> <span class="mi">1</span> <span class="o">==</span> <span class="mf">0.4</span>            <span class="c1"># False</span>
<span class="mf">5.3</span> <span class="o">%</span> <span class="mi">1</span> <span class="o">==</span> <span class="mf">0.3</span>            <span class="c1"># False</span>
<span class="mf">5.2</span> <span class="o">%</span> <span class="mi">1</span> <span class="o">==</span> <span class="mf">0.2</span>            <span class="c1"># False</span>
<span class="mf">5.1</span> <span class="o">%</span> <span class="mi">1</span> <span class="o">==</span> <span class="mf">0.1</span>            <span class="c1"># False</span>

<span class="mf">0.1</span> <span class="o">*</span> <span class="mi">1</span> <span class="o">==</span> <span class="mf">0.1</span>            <span class="c1"># True</span>
<span class="mf">0.1</span> <span class="o">*</span> <span class="mi">2</span> <span class="o">==</span> <span class="mf">0.2</span>            <span class="c1"># True</span>
<span class="mf">0.1</span> <span class="o">*</span> <span class="mi">3</span> <span class="o">==</span> <span class="mf">0.3</span>            <span class="c1"># False</span>
<span class="mf">0.1</span> <span class="o">*</span> <span class="mi">4</span> <span class="o">==</span> <span class="mf">0.4</span>            <span class="c1"># True</span>
</pre></div>
</div>
<p>As a consequence, we see that even some values that we might think are
safe to consider absolutely &quot;precise&quot; on paper are really not so exact
within the computer.  This does not mean we should avoid using such
numbers---that is really not feasible.  But it does mean that we
should adjust our assumptions as to the exactness of evaluations of
them, and we should avoid using them in certain kinds of expressions
where the approximative nature would be unstable or otherwise
problematic.  In particular, as we noted before, <strong>we should typically
not use floating point evaluations within expressions of exact
equality or inequality, because results will be unreliable.</strong></p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>In truth, calculation &quot;precision&quot; is properly defined in
terms of bytes, not decimal places, though we will often
speak of the latter.  In general, we won't have to think
about base-2 representations of numbers as we work: this is
just another point to emphasize that <em>we can't ask for
exactness with floating point numbers.</em></p>
</div>
<div class="qpractice docutils container">
<div class="line-block">
<div class="line"><strong>Q:</strong> Following on from just above, evaluate each of:</div>
<div class="line"><code class="docutils literal notranslate"><span class="pre">5.6</span> <span class="pre">%</span> <span class="pre">1</span> <span class="pre">==</span> <span class="pre">0.6</span></code>, <code class="docutils literal notranslate"><span class="pre">5.7</span> <span class="pre">%</span> <span class="pre">1</span> <span class="pre">==</span> <span class="pre">0.7</span></code>, <code class="docutils literal notranslate"><span class="pre">5.8</span> <span class="pre">%</span> <span class="pre">1</span> <span class="pre">==</span> <span class="pre">0.8</span></code> and
<code class="docutils literal notranslate"><span class="pre">5.9</span> <span class="pre">%</span> <span class="pre">1</span> <span class="pre">==</span> <span class="pre">0.9</span></code></div>
<div class="line">in Python.  Which evaluate to <code class="docutils literal notranslate"><span class="pre">True</span></code>?</div>
</div>
<blockquote>
<div><script type="text/javascript">
    function showhide(element){
        if (!document.getElementById)
            return

        if (element.style.display == "block")
            element.style.display = "none"
        else
            element.style.display = "block"
    };
</script>
<a href="javascript:showhide(document.getElementById('hiddencodeblock39'))">+ show/hide code</a><br /><div id="hiddencodeblock39" style="display: none"><div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="linenos">1</span><span class="c1"># None do.</span>
</pre></div>
</div>
</div></div></blockquote>
<div class="line-block">
<div class="line"><strong>Q:</strong> Following on from just above, evaluate each of:</div>
</div>
<div class="line-block">
<div class="line"><code class="docutils literal notranslate"><span class="pre">0.1</span> <span class="pre">*</span> <span class="pre">5</span> <span class="pre">==</span> <span class="pre">0.5</span></code>, <code class="docutils literal notranslate"><span class="pre">0.1</span> <span class="pre">*</span> <span class="pre">6</span> <span class="pre">==</span> <span class="pre">0.6</span></code>, ..., <code class="docutils literal notranslate"><span class="pre">0.1</span> <span class="pre">*</span> <span class="pre">13</span> <span class="pre">==</span>
<span class="pre">1.3</span></code></div>
<div class="line">Which evaluate to <code class="docutils literal notranslate"><span class="pre">True</span></code>?</div>
</div>
<blockquote>
<div><script type="text/javascript">
    function showhide(element){
        if (!document.getElementById)
            return

        if (element.style.display == "block")
            element.style.display = "none"
        else
            element.style.display = "block"
    };
</script>
<a href="javascript:showhide(document.getElementById('hiddencodeblock40'))">+ show/hide code</a><br /><div id="hiddencodeblock40" style="display: none"><div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="linenos">1</span><span class="c1"># The expressions that evaluate to True in that list are:</span>
<span class="linenos">2</span><span class="mf">0.1</span> <span class="o">*</span> <span class="mi">5</span> <span class="o">==</span> <span class="mf">0.5</span>
<span class="linenos">3</span><span class="mf">0.1</span> <span class="o">*</span> <span class="mi">8</span> <span class="o">==</span> <span class="mf">0.8</span>
<span class="linenos">4</span><span class="mf">0.1</span> <span class="o">*</span> <span class="mi">9</span> <span class="o">==</span> <span class="mf">0.9</span>
<span class="linenos">5</span><span class="mf">0.1</span> <span class="o">*</span> <span class="mi">10</span> <span class="o">==</span> <span class="mf">1.0</span>
<span class="linenos">6</span><span class="mf">0.1</span> <span class="o">*</span> <span class="mi">11</span> <span class="o">==</span> <span class="mf">1.1</span>
<span class="linenos">7</span><span class="mf">0.1</span> <span class="o">*</span> <span class="mi">13</span> <span class="o">==</span> <span class="mf">1.3</span>
</pre></div>
</div>
</div></div></blockquote>
</div>
</section>
<section id="practice">
<h2><a class="toc-backref" href="#id3" role="doc-backlink"><span class="section-number">12.3.  </span>Practice</a><a class="headerlink" href="#practice" title="Permalink to this heading">¶</a></h2>
<ol class="arabic">
<li><p>Does Python produce the correct answer for the following:
<code class="docutils literal notranslate"><span class="pre">0.1**2</span></code>?  Why or why not?  (Hint: &quot;correct&quot; mathematically might
differ from &quot;correct&quot; computationally.)</p></li>
<li><p>Consider having some float type variable <code class="docutils literal notranslate"><span class="pre">x</span></code>.  As noted above, we
should avoid using either one of the following equivalent
expressions for exact equality:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="n">x</span> <span class="o">%</span> <span class="mi">1</span> <span class="o">==</span> <span class="mf">0.1</span>
<span class="n">x</span> <span class="o">%</span> <span class="mi">1</span> <span class="o">-</span> <span class="mf">0.1</span> <span class="o">==</span> <span class="mi">0</span>
</pre></div>
</div>
<p>But is there an alternative way we could try to evaluate whether
<code class="docutils literal notranslate"><span class="pre">x</span> <span class="pre">%</span> <span class="pre">1</span></code> matches with <code class="docutils literal notranslate"><span class="pre">0.1</span></code> using a different comparison, which
would still allow us to tell the difference between cases where,
say, <code class="docutils literal notranslate"><span class="pre">x</span> <span class="pre">=</span> <span class="pre">5.1</span></code> and <code class="docutils literal notranslate"><span class="pre">x</span> <span class="pre">=</span> <span class="pre">5.9</span></code>?  That is, even if we cannot judge
exact equality, what might be the next best thing we could test?</p>
</li>
</ol>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<div class="sphinx-toc sphinxglobaltoc">
<h3><a href="../index.html">Table of Contents</a></h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="overview.html">1.  Overview</a></li>
<li class="toctree-l1"><a class="reference internal" href="notations.html">2.  Notations in these notes</a></li>
<li class="toctree-l1"><a class="reference internal" href="setup.html">3.  Python setup and environments</a></li>
<li class="toctree-l1"><a class="reference internal" href="basic_ops_types.html">4.  Basic operations and types</a></li>
<li class="toctree-l1"><a class="reference internal" href="assignment_var.html">5.  Assignment, variables and in-place operators</a></li>
<li class="toctree-l1"><a class="reference internal" href="comm_str_print.html">6.  Strings, comments and print: Help humans program</a></li>
<li class="toctree-l1"><a class="reference internal" href="indices.html">7.  Indices, slices and half-open intervals</a></li>
<li class="toctree-l1"><a class="reference internal" href="user_input.html">8.  User input</a></li>
<li class="toctree-l1"><a class="reference internal" href="modules.html">9.  Modules, I: Expand functionality</a></li>
<li class="toctree-l1"><a class="reference internal" href="helpdocs.html">10.  Functions, I: Read help docs and use</a></li>
<li class="toctree-l1"><a class="reference internal" href="boolean_expr.html">11.  Bools and Boolean expressions</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">12.  Be careful: Working with floats</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#binary-vs-decimal-representation-of-floats">12.1.  Binary vs decimal representation of floats</a></li>
<li class="toctree-l2"><a class="reference internal" href="#consequences-of-binary-approximation">12.2.  Consequences of binary approximation</a></li>
<li class="toctree-l2"><a class="reference internal" href="#practice">12.3.  Practice</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="conditionals.html">13.  Conditionals, I: <strong>if</strong>, <strong>else</strong> and more</a></li>
<li class="toctree-l1"><a class="reference internal" href="arrays.html">14.  Arrays, I, and indexing: 1D data</a></li>
<li class="toctree-l1"><a class="reference internal" href="plot.html">15.  Plotting, I: View 1D array(s)</a></li>
<li class="toctree-l1"><a class="reference internal" href="loop_for.html">16.  Loops, I:  <strong>for</strong></a></li>
<li class="toctree-l1"><a class="reference internal" href="methods.html">17.  Methods, I</a></li>
<li class="toctree-l1"><a class="reference internal" href="complex.html">18.  Complex type, I</a></li>
<li class="toctree-l1"><a class="reference internal" href="str_and_format.html">19.  Strings, II, and formatting</a></li>
<li class="toctree-l1"><a class="reference internal" href="list_and_list_comprehension.html">20.  Lists, I: storing more stuff</a></li>
<li class="toctree-l1"><a class="reference internal" href="cp_mutable_objs.html">21.  Be careful: copying arrays, lists (and more)</a></li>
<li class="toctree-l1"><a class="reference internal" href="functions00.html">22.  Functions, IIa: Definition and arguments</a></li>
<li class="toctree-l1"><a class="reference internal" href="functions01.html">23.  Functions, IIb: Scope of variables</a></li>
<li class="toctree-l1"><a class="reference internal" href="arrays_02.html">24.  Arrays, II</a></li>
<li class="toctree-l1"><a class="reference internal" href="importing.html">25.  Modules, II: Make and import your own</a></li>
<li class="toctree-l1"><a class="reference internal" href="file_io.html">26.  Reading and writing files</a></li>
<li class="toctree-l1"><a class="reference internal" href="conditionals_02.html">27.  Conditionals, II: Nesting</a></li>
<li class="toctree-l1"><a class="reference internal" href="loop_while_break_continue.html">28.  Loops, II:  <strong>while, break, continue</strong></a></li>
<li class="toctree-l1"><a class="reference internal" href="mod_data_sci.html">29.  Data science (with Pandas module)</a></li>
</ul>
</div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../_sources/pages/float_rep.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>document.getElementById('searchbox').style.display = "block"</script>
        </div>
      </div>
    
    
        <div class="sidebar-toggle-group no-js">
            
            <button class="sidebar-toggle" id="sidebar-hide" title="Hide the sidebar menu">
                 «
                <span class="show-for-small">hide menu</span>
                
            </button>
            <button class="sidebar-toggle" id="sidebar-show" title="Show the sidebar menu">
                
                <span class="show-for-small">menu</span>
                <span class="hide-for-small">sidebar</span>
                 »
            </button>
        </div>
    
      <div class="clearer"></div>
    </div>
    <div class="relbar-bottom">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="conditionals.html" title="13. Conditionals, I: if, else and more"
             >next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="boolean_expr.html" title="11. Bools and Boolean expressions"
             >previous</a> &nbsp; &nbsp;</li>
    <li><a href="../index.html">AIMS Python 0.7 documentation</a> &#187;</li>

        <li class="nav-item nav-item-this"><a href=""><span class="section-number">12.  </span>Be careful: Working with floats</a></li> 
      </ul>
    </div>
    </div>

    <div class="footer" role="contentinfo">
        &#169; Copyright 2020, AIMS Python Consortium.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 5.0.2.
    </div>
    <!-- cloud_sptheme 1.4 -->
  </body>
</html>