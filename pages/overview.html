


<!DOCTYPE html>

<html lang="English">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.18.1: http://docutils.sourceforge.net/" />

    <title>1. Overview &#8212; AIMS Python 0.7 documentation</title>
    <link rel="stylesheet" type="text/css" href="../_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="../_static/cloud.css" />
    <link rel="stylesheet" type="text/css" href="../_static/style.css" />
    <link rel="stylesheet" type="text/css" href="../_static/table_styling.css" />
    <link rel="stylesheet" href="../local_PT_sans" type="text/css" />
    
    <script data-url_root="../" id="documentation_options" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/_sphinx_javascript_frameworks_compat.js"></script>
    <script src="../_static/doctools.js"></script>

    
    
     
        <script src="../_static/jquery.cookie.js"></script>
    

    
     
        <script src="../_static/cloud.base.js"></script>
    

    
     
        <script src="../_static/cloud.js"></script>
    

    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="2. Notations in these notes" href="notations.html" />
    <link rel="prev" title="Welcome to AIMS Python&#39;s documentation!" href="../index.html" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
  </head><body>
    <div class="relbar-top">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="notations.html" title="2. Notations in these notes"
             accesskey="N">next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="../index.html" title="Welcome to AIMS Python&#39;s documentation!"
             accesskey="P">previous</a> &nbsp; &nbsp;</li>
    <li><a href="../index.html">AIMS Python 0.7 documentation</a> &#187;</li>

        <li class="nav-item nav-item-this"><a href=""><span class="section-number">1.  </span>Overview</a></li> 
      </ul>
    </div>
    </div>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="overview">
<span id="intro-overview"></span><h1><span class="section-number">1.  </span>Overview<a class="headerlink" href="#overview" title="Permalink to this heading">¶</a></h1>
<p>What is <strong>programming?</strong> It is a particular way for us humans to give
instructions to a computer to interpret, to calculate and to present
things.  It should not be approached as something totally new, but
instead it should be thought of as an extension of the basic,
pen-and-paper mathematics and physics you have learned from elementary
school, high school, and onwards.  In this course, we will try to
convince you that you know a lot more programming than you think you
do already.  Counting is an example, as is flipping a coin or decision
making.  If you can take the function <img class="math" src="../_images/math/e2c5a87d1353697e25d149a3824252ea1c010278.svg" alt="f(x)=x^2"/>, calculate some
values of it and plot an approximate curve, then you are already doing
a fair amount of programming.</p>
<p>An <strong>algorithm</strong> is a set of step-by-step commands to take some set of
inputs and produce something, like output(s) or a new state. We
implement algorithms in a <strong>code</strong> (or program, or script), so that
the steps can be understood by another human or by a computer using a
particular language.  When we look at pre-existing codes, they might
look complicated and hard to understand at first, but all programs are
made up of small pieces assembled together.  After you have learned
how to make the smaller pieces, it is much easier to understand how
the program works, and how to build your own program.  You might be
surprised what a few small pieces of code can accomplish together!
Ideally, programming should help you with problem-solving or
displaying data, and even allow you to express your creative side.</p>
<p>To communicate our expressions to the computer successfully, we have
to learn the correct ways to form our statements, equations and
expressions. Just as vocabulary and grammar form the way we speak and
understand everyday language, so <strong>keywords</strong> and <strong>syntax</strong>
respectively form the basis of the expressions used in a programming
language.  When communicating with other people, we can be
approximate, or assume knowledge, or make grammatical mistakes, and
our meaning might still come across OK to the other person. However,
computers are much more limited than people, and we have to use very
specific phrasing and terms in most cases.  We will spend a lot of
time here learning these syntaxes, and we need to be precise when
programming.  We also need to learn how to correct our programs (or
<strong>debug</strong> them) if we make a mistake in them.</p>
<p>We will introduce technical terms throughout these notes.  The sooner
you bring these into your own vocabulary, the better.  Using technical
terms will ultimately speed up the process of building comprehension,
asking questions, reading help files and interpreting (and fixing!)
error messages.  At the same time it is a good idea to develop your
own general/conceptual understanding of the pieces of programming: how
do <em>you</em> envision these pieces in your mind?  This is something that
everybody must do on their own, typically through lots of practice and
perhaps asking questions.  Trial-and-error helps helps delineate
things---it OK (and even <em>useful</em>) to make mistakes and to learn from
them.</p>
<p>On that note, we emphasize that <em>error messages are nothing to be
afraid of while programming</em>.  You should <em>not</em> feel bad about
seeing them.  Everyone gets them.  The Python interpreter is merely
trying to help point out where it is having trouble understanding
you---so let it, and work <em>with</em> it!  Part of programming is gaining
experience with interpreting error messages.  We try to point out
common mistakes and resulting error messages throughout these notes:
learning to interpret these messages is one key to being successful in
programming.</p>
<p>In fact, it is worse to <em>not</em> see error messages sometimes. Subtle
mistakes that don't cause warning flag are <em>much, much</em> worse than
syntax errors, since they are much harder to find.  The only way to
find these kinds of errors is to test the code by producing known
results (e.g., something we can calculate by hand, or know from some
other background knowledge).  <em>Don't just trust an output because your
program did not crash!</em> The more you know about what you <em>should</em> be
getting from a code, the more likely you are to prevent errors from
creeping in.  Ironically, using a program to solve a problem still
requires us to know a lot about it with good ol' pen and paper.</p>
<p>There is more &quot;art&quot; and flexibility to writing a program than many
people think.  It is a bit like writing a story: multiple authors can
narrate the same plot, but the way a story is organized and developed
makes a big difference in how we evaluate it.  We will cover some good
habits to incorporate into your own programming style, which should
really will help you.  Even within this loose framework, there is also
freedom to develop your own style that works for you.  Try out some
ideas in a code, and then redo them later.  But remember: you are
often programming for your future self, so be kind to that person, and
try to keep things as clear as possible.  When you give a speech, you
try to choose clear, elegant phrasing and meaningful words; why should
programming be any different?</p>
<p><em>In summary:</em></p>
<ul class="simple">
<li><p>&quot;Programming&quot; is not something totally new or alien: it is
essentially a dialect of mathematics and physics.  In fact, here we
approach programming as translating math/physics from paper to a
computer.  With this outlook, many of the syntaxes should start to
look familiar and helpful---and they were actively designed to be
so!</p></li>
<li><p>The most important part of programming takes place before you even
sit at a computer: it is formulating the problem, and any
steps/algorithm to address it, clearly on paper.</p></li>
<li><p>Don't be afraid of error/debugging messages: read them and remember
them, and after a while they become just small speedbumps in your
programming, rather than roadblocks.</p></li>
<li><p>The only way to get better at programming is with practice, so
please do follow along with the examples in these notes.  Try the
practice problems.  When something isn't clear, investigate more and
ask questions.</p></li>
<li><p>We hope programming can be enjoyable and creative.  Hopefully
solving problems, making plots and presenting data can be
interesting.  Feel free to expand any question and explore
programming further.</p></li>
</ul>
<div class="line-block">
<div class="line"><br /></div>
</div>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<div class="sphinx-toc sphinxglobaltoc">
<h3><a href="../index.html">Table of Contents</a></h3>
<ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">1.  Overview</a></li>
<li class="toctree-l1"><a class="reference internal" href="notations.html">2.  Notations in these notes</a></li>
<li class="toctree-l1"><a class="reference internal" href="setup.html">3.  Python setup and environments</a></li>
<li class="toctree-l1"><a class="reference internal" href="basic_ops_types.html">4.  Basic operations and types</a></li>
<li class="toctree-l1"><a class="reference internal" href="assignment_var.html">5.  Assignment, variables and in-place operators</a></li>
<li class="toctree-l1"><a class="reference internal" href="comm_str_print.html">6.  Strings, comments and print: Help humans program</a></li>
<li class="toctree-l1"><a class="reference internal" href="indices.html">7.  Indices, slices and half-open intervals</a></li>
<li class="toctree-l1"><a class="reference internal" href="user_input.html">8.  User input</a></li>
<li class="toctree-l1"><a class="reference internal" href="modules.html">9.  Modules, I: Expand functionality</a></li>
<li class="toctree-l1"><a class="reference internal" href="helpdocs.html">10.  Functions, I: Read help docs and use</a></li>
<li class="toctree-l1"><a class="reference internal" href="boolean_expr.html">11.  Bools and Boolean expressions</a></li>
<li class="toctree-l1"><a class="reference internal" href="float_rep.html">12.  Be careful: Working with floats</a></li>
<li class="toctree-l1"><a class="reference internal" href="conditionals.html">13.  Conditionals, I: <strong>if</strong>, <strong>else</strong> and more</a></li>
<li class="toctree-l1"><a class="reference internal" href="arrays.html">14.  Arrays, I, and indexing: 1D data</a></li>
<li class="toctree-l1"><a class="reference internal" href="plot.html">15.  Plotting, I: View 1D array(s)</a></li>
<li class="toctree-l1"><a class="reference internal" href="loop_for.html">16.  Loops, I:  <strong>for</strong></a></li>
<li class="toctree-l1"><a class="reference internal" href="methods.html">17.  Methods, I</a></li>
<li class="toctree-l1"><a class="reference internal" href="complex.html">18.  Complex type, I</a></li>
<li class="toctree-l1"><a class="reference internal" href="str_and_format.html">19.  Strings, II, and formatting</a></li>
<li class="toctree-l1"><a class="reference internal" href="list_and_list_comprehension.html">20.  Lists, I: storing more stuff</a></li>
<li class="toctree-l1"><a class="reference internal" href="cp_mutable_objs.html">21.  Be careful: copying arrays, lists (and more)</a></li>
<li class="toctree-l1"><a class="reference internal" href="functions00.html">22.  Functions, IIa: Definition and arguments</a></li>
<li class="toctree-l1"><a class="reference internal" href="functions01.html">23.  Functions, IIb: Scope of variables</a></li>
<li class="toctree-l1"><a class="reference internal" href="arrays_02.html">24.  Arrays, II</a></li>
<li class="toctree-l1"><a class="reference internal" href="importing.html">25.  Modules, II: Make and import your own</a></li>
<li class="toctree-l1"><a class="reference internal" href="file_io.html">26.  Reading and writing files</a></li>
<li class="toctree-l1"><a class="reference internal" href="conditionals_02.html">27.  Conditionals, II: Nesting</a></li>
<li class="toctree-l1"><a class="reference internal" href="loop_while_break_continue.html">28.  Loops, II:  <strong>while, break, continue</strong></a></li>
<li class="toctree-l1"><a class="reference internal" href="mod_data_sci.html">29.  Data science (with Pandas module)</a></li>
</ul>
</div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../_sources/pages/overview.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>document.getElementById('searchbox').style.display = "block"</script>
        </div>
      </div>
    
    
        <div class="sidebar-toggle-group no-js">
            
            <button class="sidebar-toggle" id="sidebar-hide" title="Hide the sidebar menu">
                 «
                <span class="show-for-small">hide menu</span>
                
            </button>
            <button class="sidebar-toggle" id="sidebar-show" title="Show the sidebar menu">
                
                <span class="show-for-small">menu</span>
                <span class="hide-for-small">sidebar</span>
                 »
            </button>
        </div>
    
      <div class="clearer"></div>
    </div>
    <div class="relbar-bottom">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="notations.html" title="2. Notations in these notes"
             >next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="../index.html" title="Welcome to AIMS Python&#39;s documentation!"
             >previous</a> &nbsp; &nbsp;</li>
    <li><a href="../index.html">AIMS Python 0.7 documentation</a> &#187;</li>

        <li class="nav-item nav-item-this"><a href=""><span class="section-number">1.  </span>Overview</a></li> 
      </ul>
    </div>
    </div>

    <div class="footer" role="contentinfo">
        &#169; Copyright 2020, AIMS Python Consortium.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 5.0.2.
    </div>
    <!-- cloud_sptheme 1.4 -->
  </body>
</html>