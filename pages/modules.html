


<!DOCTYPE html>

<html lang="English">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.18.1: http://docutils.sourceforge.net/" />

    <title>9. Modules, I: Expand functionality &#8212; AIMS Python 0.7 documentation</title>
    <link rel="stylesheet" type="text/css" href="../_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="../_static/cloud.css" />
    <link rel="stylesheet" type="text/css" href="../_static/style.css" />
    <link rel="stylesheet" type="text/css" href="../_static/table_styling.css" />
    <link rel="stylesheet" href="../local_PT_sans" type="text/css" />
    
    <script data-url_root="../" id="documentation_options" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/_sphinx_javascript_frameworks_compat.js"></script>
    <script src="../_static/doctools.js"></script>

    
    
     
        <script src="../_static/jquery.cookie.js"></script>
    

    
     
        <script src="../_static/cloud.base.js"></script>
    

    
     
        <script src="../_static/cloud.js"></script>
    

    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="10. Functions, I: Read help docs and use" href="helpdocs.html" />
    <link rel="prev" title="8. User input" href="user_input.html" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
  </head><body>
    <div class="relbar-top">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="helpdocs.html" title="10. Functions, I: Read help docs and use"
             accesskey="N">next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="user_input.html" title="8. User input"
             accesskey="P">previous</a> &nbsp; &nbsp;</li>
    <li><a href="../index.html">AIMS Python 0.7 documentation</a> &#187;</li>

        <li class="nav-item nav-item-this"><a href=""><span class="section-number">9.  </span>Modules, I: Expand functionality</a></li> 
      </ul>
    </div>
    </div>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="modules-i-expand-functionality">
<span id="modules"></span><h1><span class="section-number">9.  </span>Modules, I: Expand functionality<a class="headerlink" href="#modules-i-expand-functionality" title="Permalink to this heading">¶</a></h1>
<nav class="contents local" id="contents">
<ul class="simple">
<li><p><a class="reference internal" href="#importing-a-module" id="id1">Importing a module</a></p></li>
<li><p><a class="reference internal" href="#a-basic-set-of-modules" id="id2">A basic set of modules</a></p></li>
<li><p><a class="reference internal" href="#finding-out-more-about-modules" id="id3">Finding out more about modules</a></p></li>
<li><p><a class="reference internal" href="#looking-ahead-making-our-own-modules" id="id4">Looking ahead: making our own modules</a></p></li>
<li><p><a class="reference internal" href="#practice" id="id5">Practice</a></p></li>
</ul>
</nav>
<p><a class="reference internal" href="basic_ops_types.html#basic-ops-types"><span class="std std-ref">We have already seen</span></a> how lots of very basic
mathematical operators are directly available in Python (that is, they
are <strong>built-in</strong>).  But we would also like to be able to do further
things, like:</p>
<ul class="simple">
<li><p>calculate sinusoids, exponentials and logarithms;</p></li>
<li><p>generate random numbers from many different distributions;</p></li>
<li><p>perform image processing;</p></li>
<li><p>plot results;</p></li>
</ul>
<p>... and much more.  While it is true we could write our own
functions to do these things (and sometimes we might), in general we
can avoid &quot;reinventing the wheel&quot; and spend our time addressing other
questions, by utilizing <strong>modules</strong>.  Modules are chunks of
already-written code that contain sets of functions, constants and
other objects, generally organized around a given topic, such as
plotting or statistics or data science.</p>
<p>To use a module, we <strong>import</strong> it, a step which makes that module's
tools available within a Python program or session. Once the module
has been imported, it is just as if its code had been included at that
point in the main code.</p>
<p>We can think of importing a module like going to a library and picking
up a book on statistics for probability distributions, a book of
algebra for matrix operations, etc. and then making use of the book's
contents to enhance our own work.  In fact, sometimes modules are
referred to as <strong>libraries</strong>.  We can check books out as often as we
want, when we need them; similarly, we import a module into any
program in which it would be useful.  In the same way that always
carrying around and searching through a huge number of books would be
cumbersome, so do we just try to import the (typically few) modules we
need at a given time.</p>
<p>As with using books, we like to cite where things come from for easy
reference, for knowing where we might find more related content, and
for having better understanding by both ourselves and others.  We will
similarly find it useful to explicitly keep track of what we use from
different modules.  Additionally, sometimes there are overlaps of
functionality among modules, and we want to keep clear precisely what
is being used. These factors inform the way in which we import
modules.</p>
<section id="importing-a-module">
<h2><a class="toc-backref" href="#id1" role="doc-backlink"><span class="section-number">9.1.  </span>Importing a module</a><a class="headerlink" href="#importing-a-module" title="Permalink to this heading">¶</a></h2>
<p>There are different ways to import a module (or even just pieces of a
module).  In general, we strongly prefer the following method (with a
couple minor variations) and will typically use it here:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span> <span class="o">&lt;</span><span class="n">module</span> <span class="n">name</span><span class="o">&gt;</span> <span class="k">as</span> <span class="o">&lt;</span><span class="n">abbreviation</span><span class="o">&gt;</span>
</pre></div>
</div>
<p>The abbreviation will be chosen to be unique for each module in a
program, and it is included every time we use a feature from the
module.  In this way, it acts as a reference citation both for
ourselves and for others.  It also helps avoid problems of conflicts
if, for example, two modules contain a function of the same name but
perhaps different syntax; using the distinct abbreviation
disambiguates which function we actually use.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>For some very popular modules, there are commonly used
abbreviations that are implemented in most texts, help
documentation, and shared code. We will typically use these,
because it makes understandability, integration and
translation easier.</p>
</div>
<p>For example, one of the most common modules we will make use of is
NumPy (or simply &quot;numpy&quot;; <a class="reference external" href="http://www.numpy.org/">link</a>).  It
contains many useful numerical computing tools, such as: arrays
(analogous to vectors/tensors in math), linear algebra operations,
trigonometric and hyperbolic functions, random number generators, and
much more.  The standard importation and abbreviation syntax is:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span> <span class="nn">numpy</span> <span class="k">as</span> <span class="nn">np</span>
</pre></div>
</div>
<p>Once this is done, to use any function or attribute within numpy, we
type the name of the module, then <code class="docutils literal notranslate"><span class="pre">.</span></code>, then the name of the function
or constant in order to use it.  For example, the following shows ways
to use various sinusoid, exponential and logarithmic functions, as
well as utilize the famous constant <img class="math" src="../_images/math/72e246b58b3ce3564045871ee69045dc9c1d1e42.svg" alt="\pi"/>:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="n">np</span><span class="o">.</span><span class="n">sin</span><span class="p">(</span><span class="mi">10</span><span class="p">)</span>
<span class="n">x</span> <span class="o">=</span> <span class="n">np</span><span class="o">.</span><span class="n">exp</span><span class="p">(</span><span class="o">-</span><span class="mf">3.5</span><span class="p">)</span>
<span class="n">y</span> <span class="o">=</span> <span class="n">np</span><span class="o">.</span><span class="n">log</span><span class="p">(</span><span class="n">x</span><span class="p">)</span>
<span class="n">np</span><span class="o">.</span><span class="n">log</span><span class="p">(</span><span class="mi">10</span><span class="o">**</span><span class="mi">7</span><span class="p">)</span>
<span class="nb">print</span><span class="p">(</span><span class="s2">&quot;The value of PI is:&quot;</span><span class="p">,</span> <span class="n">np</span><span class="o">.</span><span class="n">pi</span><span class="p">)</span>
</pre></div>
</div>
<p>Some large modules even contain submodules for specific topics. We can
directly import the submodule in just the same manner as the full
module, providing it its own abbreviation.  For example, we will often
use the &quot;pyplot&quot; submodule of Matplotlib for making plots and graphs:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span> <span class="nn">matplotlib.pyplot</span> <span class="k">as</span> <span class="nn">plt</span>
</pre></div>
</div>
<p>Again, functions within the (sub)module are just used in the same
manner as before, referencing the abbreviation:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="n">plt</span><span class="o">.</span><span class="n">plot</span><span class="p">([</span><span class="mi">1</span><span class="p">,</span> <span class="mi">2</span><span class="p">,</span> <span class="mi">3</span><span class="p">],</span> <span class="p">[</span><span class="mi">6</span><span class="p">,</span> <span class="mi">2</span><span class="p">,</span> <span class="mi">6</span><span class="p">])</span>
<span class="n">plt</span><span class="o">.</span><span class="n">show</span><span class="p">()</span>
</pre></div>
</div>
<p>Some modules have very short names to start with, so we just use their
full name as a reference (no extra abbreviation). Or sometimes the
name isn't that short, but the full name is just used because of
historical convention.  For example, <code class="docutils literal notranslate"><span class="pre">sys</span></code> contains system
functions (such as exiting a program); <code class="docutils literal notranslate"><span class="pre">os</span></code> contains operating
system (OS) routines, such as getting file paths; and <code class="docutils literal notranslate"><span class="pre">subprocess</span></code>
can run and/or check commands in the system shell.  These are
traditionally just imported as:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span> <span class="nn">sys</span>
<span class="kn">import</span> <span class="nn">os</span>
<span class="kn">import</span> <span class="nn">subprocess</span>
</pre></div>
</div>
<p>which means that they or their submodules are referenced by their full
name:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="n">sys</span><span class="o">.</span><span class="n">version</span>                <span class="c1"># show current Python version number</span>
<span class="n">os</span><span class="o">.</span><span class="n">path</span><span class="o">.</span><span class="n">abspath</span><span class="p">(</span><span class="s1">&#39;.&#39;</span><span class="p">)</span>       <span class="c1"># show present working directory on computer (via &#39;path&#39; submodule)</span>
<span class="n">subprocess</span><span class="o">.</span><span class="n">run</span><span class="p">(</span><span class="s1">&#39;ls&#39;</span><span class="p">)</span>       <span class="c1"># run the terminal command &quot;ls&quot;, showing directory contents</span>
</pre></div>
</div>
</section>
<section id="a-basic-set-of-modules">
<h2><a class="toc-backref" href="#id2" role="doc-backlink"><span class="section-number">9.2.  </span>A basic set of modules</a><a class="headerlink" href="#a-basic-set-of-modules" title="Permalink to this heading">¶</a></h2>
<p>In this course we will make use of the following modules.  Again, this
is a tiny set in an increasing set of modules available within the
Python universe.  (They should be installed on your systems
already---if they aren't, and you receive an error message at import,
please inquire.)</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 15%" />
<col style="width: 40%" />
<col style="width: 45%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Module/link</p></th>
<th class="head"><p>Standard import</p></th>
<th class="head"><p>Examples of functionality (partial!)</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><a class="reference external" href="https://docs.python.org/3/library/math.html">Math</a></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">math</span></code></p></td>
<td><p>basic mathematical functions (sinusoids, logs, exp, floor,
ceiling, abs val, etc.)</p></td>
</tr>
<tr class="row-odd"><td><p><a class="reference external" href="https://docs.scipy.org/doc/numpy/reference/index.html#reference">NumPy</a></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">numpy</span> <span class="pre">as</span> <span class="pre">np</span></code></p></td>
<td><p>basic math, linear algebra, arrays, matrix operations, random
numbers, index operations, polynomials, Fourier transform,
array-wise calculations</p></td>
</tr>
<tr class="row-even"><td><p><a class="reference external" href="https://docs.scipy.org/doc/scipy/reference/">SciPy</a></p></td>
<td><div class="line-block">
<div class="line"><code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">scipy.integrate</span> <span class="pre">as</span> <span class="pre">integrate</span></code></div>
<div class="line"><code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">scipy.special</span> <span class="pre">as</span> <span class="pre">special</span></code></div>
</div>
</td>
<td><p>interpolation, integration, linear algebra, dealing with sparse
matrices, image processing, optimization</p></td>
</tr>
<tr class="row-odd"><td><p><a class="reference external" href="https://matplotlib.org/">Matplotlib</a></p></td>
<td><div class="line-block">
<div class="line"><code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">matplotlib</span> <span class="pre">as</span> <span class="pre">mpl</span></code></div>
<div class="line"><code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">matplotlib.pyplot</span> <span class="pre">as</span> <span class="pre">plt</span></code></div>
</div>
</td>
<td><p>plotting (2D and 3D), histograms, graphs, charts, image
processing, interactive plots</p></td>
</tr>
<tr class="row-even"><td><p><a class="reference external" href="https://docs.python.org/3/library/random.html">Random</a></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">random</span></code></p></td>
<td><p>random number generation of various distributions and styles</p></td>
</tr>
<tr class="row-odd"><td><p><a class="reference external" href="https://pandas.pydata.org/docs/">Pandas</a></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">pandas</span> <span class="pre">as</span> <span class="pre">pd</span></code></p></td>
<td><p>data science, using dataframes</p></td>
</tr>
<tr class="row-even"><td><p><a class="reference external" href="https://docs.python.org/3/library/sys.html">sys</a></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">sys</span></code></p></td>
<td><p>system functions, reading in arguments, controlled exiting</p></td>
</tr>
<tr class="row-odd"><td><p><a class="reference external" href="https://docs.python.org/3/library/os.html">os</a></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">os</span></code></p></td>
<td><p>interfacing with the operating system (OS), getting file paths,
seeing directory structure</p></td>
</tr>
<tr class="row-even"><td><p><a class="reference external" href="https://docs.python.org/3/library/copy.html">copy</a></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">copy</span></code></p></td>
<td><p>copying structures (in a way to duplicate the structure, not
just the label)</p></td>
</tr>
<tr class="row-odd"><td><p><a class="reference external" href="https://docs.python.org/3/library/time.html">time</a></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">time</span></code></p></td>
<td><p>getting clock time, date-time, calendar information</p></td>
</tr>
<tr class="row-even"><td><p><a class="reference external" href="https://docs.python.org/3/library/turtle.html">turtle</a></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">turtle</span></code></p></td>
<td><p>graphical interface for demonstrating how to implement loops,
conditions, etc.</p></td>
</tr>
</tbody>
</table>
<p>We will often include the import statement in examples in these notes.
However, there will also be cases where we do not, and <strong>from this
point onward, we will expect that you recognize the module
abbreviations noted here, and know to import the necessary library.</strong>
Thus, if you see:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="n">math</span><span class="o">.</span><span class="n">sin</span><span class="p">(</span><span class="n">math</span><span class="o">.</span><span class="n">pi</span> <span class="o">/</span> <span class="mi">4</span><span class="p">)</span>
</pre></div>
</div>
<p>... you should understand that your code or Python session <em>must</em>
include <code class="docutils literal notranslate"><span class="pre">import</span> <span class="pre">math</span></code> prior to that line.  If there is any doubt or
question, you can check the <code class="docutils literal notranslate"><span class="pre">import</span></code> statements at the top of the
file, and see where a given module (or submodule) has come from.  For
example, this check can be useful if you don't recognize an module's
abbreviation.</p>
<p>The <code class="docutils literal notranslate"><span class="pre">math</span></code> and <code class="docutils literal notranslate"><span class="pre">numpy</span></code> modules have a large amount of overlapping
functionality, with the former's basically a subset of the latter's.
<code class="docutils literal notranslate"><span class="pre">math</span></code> is distributed with Python (so no need to install it
separately, but it still needs to be imported), and we will find it
more convenient to use at the start, due to its simpler structure.
Later on, we will migrate to using <code class="docutils literal notranslate"><span class="pre">numpy</span></code>, because it has a broader
and more powerful range of applications, in particular for
calculations with arrays and matrices.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>The term <strong>package</strong> technically applies to a set of
modules, while a module is contained in a single file
somewhere on the computer.  However, there really isn't any
distinction between importing a module or package, so we
might fall into the (technically bad) habit of referring to
them interchangeably, even if it is slightly inaccurate.</p>
</div>
</section>
<section id="finding-out-more-about-modules">
<h2><a class="toc-backref" href="#id3" role="doc-backlink"><span class="section-number">9.3.  </span>Finding out more about modules</a><a class="headerlink" href="#finding-out-more-about-modules" title="Permalink to this heading">¶</a></h2>
<div class="line-block">
<div class="line"><strong>How can we tell what functionality is available in a module?</strong></div>
<div class="line">We can:</div>
</div>
<ul>
<li><p>read the online documentation, such as provided in the above table
listing modules,</p></li>
<li><p>use TAB-autocompletion: after importing a module, type its
abbreviation and <code class="docutils literal notranslate"><span class="pre">.</span></code> (e.g., <code class="docutils literal notranslate"><span class="pre">math.</span></code>) and then hit <code class="docutils literal notranslate"><span class="pre">TAB</span></code>; a
browseable list of the module's contents will appear, and you can
start navigating or typing for specific functionality,</p></li>
<li><p>guess a name and try typing it after <code class="docutils literal notranslate"><span class="pre">math.</span></code>. Many module function
names are the same as known mathematical functions (e.g., sin(),
cos(), arctan(), tanh(), etc.) or variations (e.g., log(), log10(),
log2(), etc.), and TAB-autocompletion might help show options when
we have typed part of a known name,</p></li>
<li><p>type <code class="docutils literal notranslate"><span class="pre">dir(math)</span></code> to see a list of all objects in the module,</p></li>
<li><div class="line-block">
<div class="line">search online for the name of the function, such as with:</div>
<div class="line"><em>python module plotting</em>.</div>
</div>
</li>
</ul>
<div class="line-block">
<div class="line"><strong>How can we find what module contains some particular function?</strong></div>
<div class="line">We can:</div>
</div>
<ul>
<li><p>know from experience as we program (that is why practicing
matters!),</p></li>
<li><p>note the abbreviation prefixing the function's name in a code, and
check the <code class="docutils literal notranslate"><span class="pre">import</span></code> statement,</p></li>
<li><div class="line-block">
<div class="line">search online, such as:</div>
<div class="line"><em>python module binomial distribution</em></div>
<div class="line">or more generally,</div>
<div class="line"><em>python module &lt;function description&gt;</em></div>
</div>
</li>
</ul>
<div class="line-block">
<div class="line"><strong>How do we know the syntax and usage of a function in a module?</strong></div>
<div class="line">We can:</div>
</div>
<ul class="simple">
<li><p>read the function's internal help, as discussed <a class="reference internal" href="helpdocs.html#helpdocs"><span class="std std-ref">in the next
section</span></a>. All built-in objects in Python and those
in distributed modules come with help documentation.  When we write
our own modules, we can also include our own help documentation with
it.</p></li>
<li><p>... and we can also look online for examples and help
documentatoin, some of which has been provided above in the main
module table.</p></li>
</ul>
<div class="qpractice docutils container">
<p><strong>Q:</strong> How many functions and attributes within the <code class="docutils literal notranslate"><span class="pre">math</span></code> module
start with the letter &quot;p&quot;?  What are they?</p>
<script type="text/javascript">
    function showhide(element){
        if (!document.getElementById)
            return

        if (element.style.display == "block")
            element.style.display = "none"
        else
            element.style.display = "block"
    };
</script>
<a href="javascript:showhide(document.getElementById('hiddencodeblock73'))">+ show/hide response</a><br /><div id="hiddencodeblock73" style="display: none"><div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="c1"># There are 4 (at least in my version of ``math``). They are:</span>
<span class="c1">#   perm()  pi   pow()  prod()</span>

<span class="c1"># To find these, I ran:</span>
<span class="kn">import</span> <span class="nn">math</span>
<span class="c1"># Then I typed:</span>
<span class="n">math</span><span class="o">.</span>
<span class="c1"># ... *without* hitting Enter, but instead hitting TAB a couple</span>
<span class="c1"># times, and the list of four possible items appeared.</span>

<span class="c1"># It is also possible to type:</span>
<span class="nb">dir</span><span class="p">(</span><span class="n">math</span><span class="p">)</span>
<span class="c1"># ... and check the displayed list of feature names.</span>
</pre></div>
</div>
</div></div>
</section>
<section id="looking-ahead-making-our-own-modules">
<h2><a class="toc-backref" href="#id4" role="doc-backlink"><span class="section-number">9.4.  </span>Looking ahead: making our own modules</a><a class="headerlink" href="#looking-ahead-making-our-own-modules" title="Permalink to this heading">¶</a></h2>
<p>Additionally, we can write our <em>own</em> modules to help us organize our
work.  A module can be as simple as a text file with function
definitions, parameter assignments and other objects of interest.  We
would import such a module in basically the same way as the other
modules, shown above (and we would still use an abbreviation for each
one).</p>
<p>Making our own modules allow us to write code, and separate it out
from other code into a different file. This style of
programming---having distinct pieces for specific jobs, which are then
later combined-- is one example of <strong>modular programming</strong>.  This is a
very efficient model for being able to read, test, verify and add to
smaller pieces of code, which are then combined into the larger
product.</p>
<p>We can use the import statement to include the code of another file,
either in the current directory or in Python's search path.  For
example, if we have some functions defined in a file called
&quot;lib_astro_functions.py&quot; (and it is either in the present working
directory or in Python's search path), then we can put the following
in our main code:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span> <span class="nn">lib_astro_functions</span> <span class="k">as</span> <span class="nn">laf</span>
</pre></div>
</div>
<p>and then use functions from it by prefixing them with <code class="docutils literal notranslate"><span class="pre">laf.</span></code>, such
as <code class="docutils literal notranslate"><span class="pre">laf.solar_radius</span></code>, <code class="docutils literal notranslate"><span class="pre">laf.calc_gravity_strength()</span></code>,
<code class="docutils literal notranslate"><span class="pre">laf.convert_hubble_to_SI()</span></code>, or any other hypothetical constants
and functions.  Note that the <code class="docutils literal notranslate"><span class="pre">.py</span></code> of the library text file's name
is left out when importing it-- just part of the <code class="docutils literal notranslate"><span class="pre">import</span></code>
statement's syntax.  (In the case of self-written modules, we likely
have to make up our own abbreviation, and the above was just chosen
from the initials of the file name.)</p>
<p>Rather than have one really big book of all of mathematics and
mathematical sciences, it is useful to keep different areas
compartmentalized for ease of reference and use.  The same is true of
programming: we don't want a single, huge file that is hard to search
through or to check individual pieces of.  Modularization makes
programming more manageable (and if you don't believe us now, you
<em>will</em> later!).</p>
</section>
<section id="practice">
<h2><a class="toc-backref" href="#id5" role="doc-backlink"><span class="section-number">9.5.  </span>Practice</a><a class="headerlink" href="#practice" title="Permalink to this heading">¶</a></h2>
<p><em>NB: When using any function, one has to know things like the number
of inputs needed, the expected units of inputs (e.g., radians or
degrees, for trigonometric functions), and other usage notes.  Knowing
these important points requires reading the help description of a
function, which we cover in the next section.  In programming, we</em>
never <em>want to have to guess that something is correct, but for these
few questions, assume that each function takes a single input and that
all units are &quot;correct&quot;.</em></p>
<ol class="arabic">
<li><p>How many functions in the <code class="docutils literal notranslate"><span class="pre">math</span></code> module start with &quot;log&quot;?</p></li>
<li><p>Write the Python statement to evaluate and print <img class="math" src="../_images/math/4f82c9406ff2d4c29d42f9b7e29ac93773f9f71d.svg" alt="y =
\tan(\pi^2)"/>. Note that you need to import the correct module.</p></li>
<li><p>The mathematical operation of &quot;ceiling&quot; a value <code class="docutils literal notranslate"><span class="pre">v</span></code> is written as
<img class="math" src="../_images/math/102434395686128acb84f367eb3f3cc0ebd9ed70.svg" alt="\left\lceil{v}\right\rceil"/>, and that of &quot;flooring&quot; is
<img class="math" src="../_images/math/dcfb2b6188464ac90b3006b9e24851e2d92b5150.svg" alt="\left\lfloor{v}\right\rfloor"/>. Find the Python functions for
each these operations, and see how each behaves on positive and
negative float values.</p></li>
<li><p>Import the math module, ask the user for a number <code class="docutils literal notranslate"><span class="pre">a</span></code>, and then
print the evaluation of:</p>
<div class="math">
<p><img src="../_images/math/2eaec2008e408ddffb0956d1cd23ddaa31191263.svg" alt="\dfrac{\cos(a)\,\log_{10}(1+a^2)}{1+e^{-a}}"/></p>
</div></li>
<li><p>Import the math module, ask the user for a float <code class="docutils literal notranslate"><span class="pre">x</span></code>, and then
print the evaluation of:</p>
<div class="math">
<p><img src="../_images/math/94242ebb2789abf5e718210f0144d8e43a4c8353.svg" alt="y = \frac{1}{\sigma\sqrt{2\pi}}\,e^{-\frac{(x-\mu)^2}{2\sigma^2}}"/></p>
</div><p>for <img class="math" src="../_images/math/b933f0fa5740b50e4ea745a2dcde20c83caa449a.svg" alt="\sigma = 1"/> and <img class="math" src="../_images/math/248621e2f231c35de1a75b82f8dd3817f0a00665.svg" alt="\mu = 0"/>.</p>
</li>
</ol>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<div class="sphinx-toc sphinxglobaltoc">
<h3><a href="../index.html">Table of Contents</a></h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="overview.html">1.  Overview</a></li>
<li class="toctree-l1"><a class="reference internal" href="notations.html">2.  Notations in these notes</a></li>
<li class="toctree-l1"><a class="reference internal" href="setup.html">3.  Python setup and environments</a></li>
<li class="toctree-l1"><a class="reference internal" href="basic_ops_types.html">4.  Basic operations and types</a></li>
<li class="toctree-l1"><a class="reference internal" href="assignment_var.html">5.  Assignment, variables and in-place operators</a></li>
<li class="toctree-l1"><a class="reference internal" href="comm_str_print.html">6.  Strings, comments and print: Help humans program</a></li>
<li class="toctree-l1"><a class="reference internal" href="indices.html">7.  Indices, slices and half-open intervals</a></li>
<li class="toctree-l1"><a class="reference internal" href="user_input.html">8.  User input</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">9.  Modules, I: Expand functionality</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#importing-a-module">9.1.  Importing a module</a></li>
<li class="toctree-l2"><a class="reference internal" href="#a-basic-set-of-modules">9.2.  A basic set of modules</a></li>
<li class="toctree-l2"><a class="reference internal" href="#finding-out-more-about-modules">9.3.  Finding out more about modules</a></li>
<li class="toctree-l2"><a class="reference internal" href="#looking-ahead-making-our-own-modules">9.4.  Looking ahead: making our own modules</a></li>
<li class="toctree-l2"><a class="reference internal" href="#practice">9.5.  Practice</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="helpdocs.html">10.  Functions, I: Read help docs and use</a></li>
<li class="toctree-l1"><a class="reference internal" href="boolean_expr.html">11.  Bools and Boolean expressions</a></li>
<li class="toctree-l1"><a class="reference internal" href="float_rep.html">12.  Be careful: Working with floats</a></li>
<li class="toctree-l1"><a class="reference internal" href="conditionals.html">13.  Conditionals, I: <strong>if</strong>, <strong>else</strong> and more</a></li>
<li class="toctree-l1"><a class="reference internal" href="arrays.html">14.  Arrays, I, and indexing: 1D data</a></li>
<li class="toctree-l1"><a class="reference internal" href="plot.html">15.  Plotting, I: View 1D array(s)</a></li>
<li class="toctree-l1"><a class="reference internal" href="loop_for.html">16.  Loops, I:  <strong>for</strong></a></li>
<li class="toctree-l1"><a class="reference internal" href="methods.html">17.  Methods, I</a></li>
<li class="toctree-l1"><a class="reference internal" href="complex.html">18.  Complex type, I</a></li>
<li class="toctree-l1"><a class="reference internal" href="str_and_format.html">19.  Strings, II, and formatting</a></li>
<li class="toctree-l1"><a class="reference internal" href="list_and_list_comprehension.html">20.  Lists, I: storing more stuff</a></li>
<li class="toctree-l1"><a class="reference internal" href="cp_mutable_objs.html">21.  Be careful: copying arrays, lists (and more)</a></li>
<li class="toctree-l1"><a class="reference internal" href="functions00.html">22.  Functions, IIa: Definition and arguments</a></li>
<li class="toctree-l1"><a class="reference internal" href="functions01.html">23.  Functions, IIb: Scope of variables</a></li>
<li class="toctree-l1"><a class="reference internal" href="arrays_02.html">24.  Arrays, II</a></li>
<li class="toctree-l1"><a class="reference internal" href="importing.html">25.  Modules, II: Make and import your own</a></li>
<li class="toctree-l1"><a class="reference internal" href="file_io.html">26.  Reading and writing files</a></li>
<li class="toctree-l1"><a class="reference internal" href="conditionals_02.html">27.  Conditionals, II: Nesting</a></li>
<li class="toctree-l1"><a class="reference internal" href="loop_while_break_continue.html">28.  Loops, II:  <strong>while, break, continue</strong></a></li>
<li class="toctree-l1"><a class="reference internal" href="mod_data_sci.html">29.  Data science (with Pandas module)</a></li>
</ul>
</div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../_sources/pages/modules.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>document.getElementById('searchbox').style.display = "block"</script>
        </div>
      </div>
    
    
        <div class="sidebar-toggle-group no-js">
            
            <button class="sidebar-toggle" id="sidebar-hide" title="Hide the sidebar menu">
                 «
                <span class="show-for-small">hide menu</span>
                
            </button>
            <button class="sidebar-toggle" id="sidebar-show" title="Show the sidebar menu">
                
                <span class="show-for-small">menu</span>
                <span class="hide-for-small">sidebar</span>
                 »
            </button>
        </div>
    
      <div class="clearer"></div>
    </div>
    <div class="relbar-bottom">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="helpdocs.html" title="10. Functions, I: Read help docs and use"
             >next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="user_input.html" title="8. User input"
             >previous</a> &nbsp; &nbsp;</li>
    <li><a href="../index.html">AIMS Python 0.7 documentation</a> &#187;</li>

        <li class="nav-item nav-item-this"><a href=""><span class="section-number">9.  </span>Modules, I: Expand functionality</a></li> 
      </ul>
    </div>
    </div>

    <div class="footer" role="contentinfo">
        &#169; Copyright 2020, AIMS Python Consortium.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 5.0.2.
    </div>
    <!-- cloud_sptheme 1.4 -->
  </body>
</html>