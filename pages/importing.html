


<!DOCTYPE html>

<html lang="English">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.18.1: http://docutils.sourceforge.net/" />

    <title>25. Modules, II: Make and import your own &#8212; AIMS Python 0.7 documentation</title>
    <link rel="stylesheet" type="text/css" href="../_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="../_static/cloud.css" />
    <link rel="stylesheet" type="text/css" href="../_static/style.css" />
    <link rel="stylesheet" type="text/css" href="../_static/table_styling.css" />
    <link rel="stylesheet" href="../local_PT_sans" type="text/css" />
    
    <script data-url_root="../" id="documentation_options" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/_sphinx_javascript_frameworks_compat.js"></script>
    <script src="../_static/doctools.js"></script>

    
    
     
        <script src="../_static/jquery.cookie.js"></script>
    

    
     
        <script src="../_static/cloud.base.js"></script>
    

    
     
        <script src="../_static/cloud.js"></script>
    

    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="26. Reading and writing files" href="file_io.html" />
    <link rel="prev" title="24. Arrays, II" href="arrays_02.html" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
  </head><body>
    <div class="relbar-top">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="file_io.html" title="26. Reading and writing files"
             accesskey="N">next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="arrays_02.html" title="24. Arrays, II"
             accesskey="P">previous</a> &nbsp; &nbsp;</li>
    <li><a href="../index.html">AIMS Python 0.7 documentation</a> &#187;</li>

        <li class="nav-item nav-item-this"><a href=""><span class="section-number">25.  </span>Modules, II: Make and import your own</a></li> 
      </ul>
    </div>
    </div>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="modules-ii-make-and-import-your-own">
<span id="importing"></span><h1><span class="section-number">25.  </span>Modules, II: Make and import your own<a class="headerlink" href="#modules-ii-make-and-import-your-own" title="Permalink to this heading">¶</a></h1>
<nav class="contents local" id="contents">
<ul class="simple">
<li><p><a class="reference internal" href="#modules-and-modularity" id="id1">Modules and modularity</a></p></li>
<li><p><a class="reference internal" href="#making-a-module-in-python" id="id2">Making a module in Python</a></p></li>
<li><p><a class="reference internal" href="#example-files-to-import" id="id3">Example files to import</a></p></li>
</ul>
</nav>
<section id="modules-and-modularity">
<h2><a class="toc-backref" href="#id1" role="doc-backlink"><span class="section-number">25.1.  </span>Modules and modularity</a><a class="headerlink" href="#modules-and-modularity" title="Permalink to this heading">¶</a></h2>
<p>We have seen the utility of importing modules in Python.  This allows
us to have a lot of related functions and functionality stored in one
place, which we can access all at once by importing into our own
program, notebook, etc.  Separating a project into smaller pieces of
related items is referred to as <strong>modularity</strong>.  Designing one's work
in this way is extremely useful as projects get larger, more
complicated and/or involve more people.</p>
<p>Python itself is organized this way from the highest levels.  Not all
functions just exist in the Python environment-- they are located in
separate modules.  For example, we have used the NumPy module quite
often, importing it with:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span> <span class="nn">numpy</span> <span class="k">as</span> <span class="nn">np</span>
</pre></div>
</div>
<p>and then calling functions contained in it with <code class="docutils literal notranslate"><span class="pre">np.sin(...)</span></code>,
<code class="docutils literal notranslate"><span class="pre">np.log10(...)</span></code>, <code class="docutils literal notranslate"><span class="pre">np.random.randint(...)</span></code>, etc.</p>
<p>Ultimately, a module is just a file (or set of files) containing the
definitions of the functions and other objects.  &quot;Installing&quot; modules
when setting up Python typically just means putting those files
somewhere in Python's system path where they can be found directly.</p>
<p>There are many reasons why we might want to make our own module of
functions, constants and other objects.  For example:</p>
<ul class="simple">
<li><p>We might want to use the same functions in different programs of our
own, and it is easier to have a function in just one file to
maintain/update/fix rather than trying to remember to make the same
changes across several copies.</p></li>
<li><p>We might be working on a collaborative project, and share the
defined objects with other people.</p></li>
<li><p>If there are a lot of functions, having a separate file of
definitions keeps the file/notebook where we are calling the
functions much smaller and more readable.</p></li>
</ul>
<p>In general, as one works on larger projects, one finds that having
&quot;general definitions&quot; stored together on their own, separately from
&quot;specific usages&quot; (calling functions, making intermediate variables,
etc.), very useful.</p>
</section>
<section id="making-a-module-in-python">
<h2><a class="toc-backref" href="#id2" role="doc-backlink"><span class="section-number">25.2.  </span>Making a module in Python</a><a class="headerlink" href="#making-a-module-in-python" title="Permalink to this heading">¶</a></h2>
<p>At its most basic, one can make a module just by putting some
definitions and/or assignments in a file.  Then, in any file located
in the same directory as the module file, one can import it using the
name of file.</p>
<p>For example, we can construct a file called &quot;my_module.py&quot; (it must
end with the extension &quot;.py), which contains the following lines:</p>
<blockquote>
<div><div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="c1"># Ex. module:  my_module.py</span>
<span class="c1"># Version   :  1.0</span>
<span class="c1"># Date      :  Feb. 2, 2018</span>

<span class="n">xval</span> <span class="o">=</span> <span class="mi">10</span>

<span class="k">def</span> <span class="nf">f_hello</span><span class="p">():</span>
    <span class="nb">print</span><span class="p">(</span><span class="s2">&quot;Hello!&quot;</span><span class="p">)</span>
</pre></div>
</div>
</div></blockquote>
<p>Then in any Python file, notebook or environment in the same directory
we can type a standard import command, just like what is used for
NumPy or Matplotlib, with some appropriate abbreviation:</p>
<blockquote>
<div><div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span> <span class="nn">my_module</span> <span class="k">as</span> <span class="nn">mm</span>
</pre></div>
</div>
</div></blockquote>
<p>(noting that the &quot;.py&quot; extension of the file name is not included in
the <code class="docutils literal notranslate"><span class="pre">import</span></code> command; it is assumed by Python).  After that, in the
program we can make use of the <code class="docutils literal notranslate"><span class="pre">xval</span></code> and function <code class="docutils literal notranslate"><span class="pre">f_hello()</span></code> by
calling them as:</p>
<blockquote>
<div><div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="nb">print</span><span class="p">(</span><span class="s2">&quot;the imported value is:&quot;</span><span class="p">,</span> <span class="n">mm</span><span class="o">.</span><span class="n">xval</span><span class="p">)</span>

<span class="n">mm</span><span class="o">.</span><span class="n">f_hello</span><span class="p">()</span>
</pre></div>
</div>
</div></blockquote>
<p><em>Ta da!</em></p>
</section>
<section id="example-files-to-import">
<h2><a class="toc-backref" href="#id3" role="doc-backlink"><span class="section-number">25.3.  </span>Example files to import</a><a class="headerlink" href="#example-files-to-import" title="Permalink to this heading">¶</a></h2>
<p>There are a couple of points about importing a file as a module. One
can organize the file to import some contents (like general
definitions, constants, etc.) while ignoring some others (specific
usage, tests, intermediate quantities).  This is done most directly by
using a special variable defined by the Python interpreter called
<code class="docutils literal notranslate"><span class="pre">__name__</span></code> (yes, note the multiple underscores).</p>
<p>Basically, the Python interpreter will define <code class="docutils literal notranslate"><span class="pre">__name__</span></code> to be
<code class="docutils literal notranslate"><span class="pre">__main__</span></code> when running/executing a file directly; however, when
importing a module, it will make it the module's name.  Therefore,
this differentiation can be used to place some lines of code that we
want run <em>only</em> when executing the file but <em>not</em> when importing it,
via an &quot;if&quot; condition.  See the difference between importing each of
the following files:</p>
<p>You can download the displayed program examples here:</p>
<ul class="simple">
<li><p><a class="reference download internal" download="" href="../_downloads/a7f95b294430bc84283ef4fb4731e842/prog_file_00.py"><code class="xref download docutils literal notranslate"><span class="pre">prog_file_00.py</span></code></a></p></li>
<li><p><a class="reference download internal" download="" href="../_downloads/70a8cdd383f19e533ca357737f255fea/prog_file_01.py"><code class="xref download docutils literal notranslate"><span class="pre">prog_file_01.py</span></code></a></p></li>
</ul>
<p>Echoed text of the files:</p>
<blockquote>
<div><table class="docutils align-default">
<colgroup>
<col style="width: 100%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>File <strong>prog_file_00.py</strong>:</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="linenos"> 1</span><span class="c1"># Example program for importing</span>
<span class="linenos"> 2</span><span class="c1"># *not* using __name__</span>
<span class="linenos"> 3</span>
<span class="linenos"> 4</span><span class="kn">import</span> <span class="nn">numpy</span> <span class="k">as</span> <span class="nn">np</span>
<span class="linenos"> 5</span>
<span class="linenos"> 6</span>
<span class="linenos"> 7</span><span class="k">def</span> <span class="nf">func01_max</span><span class="p">(</span><span class="n">x</span><span class="p">,</span> <span class="n">y</span><span class="p">):</span>
<span class="linenos"> 8</span>    <span class="sd">&#39;&#39;&#39;func01_max(x, y)</span>
<span class="linenos"> 9</span>
<span class="linenos">10</span><span class="sd">    This program takes two inputs and compares them to find</span>
<span class="linenos">11</span><span class="sd">    the maximum.  </span>
<span class="linenos">12</span>
<span class="linenos">13</span><span class="sd">    The maximum value is output; if they are equal, 0 is returned.&#39;&#39;&#39;</span>
<span class="linenos">14</span>    
<span class="linenos">15</span>    <span class="k">if</span> <span class="n">x</span> <span class="o">&gt;</span> <span class="n">y</span><span class="p">:</span>
<span class="linenos">16</span>        <span class="nb">print</span><span class="p">(</span><span class="s2">&quot;first value is greater&quot;</span><span class="p">)</span>
<span class="linenos">17</span>        <span class="k">return</span> <span class="n">x</span>
<span class="linenos">18</span>    <span class="k">elif</span> <span class="n">y</span> <span class="o">&gt;</span> <span class="n">x</span><span class="p">:</span>
<span class="linenos">19</span>        <span class="nb">print</span><span class="p">(</span><span class="s2">&quot;second value is greater&quot;</span><span class="p">)</span>
<span class="linenos">20</span>        <span class="k">return</span> <span class="n">y</span>
<span class="linenos">21</span>    <span class="k">else</span><span class="p">:</span>
<span class="linenos">22</span>        <span class="nb">print</span><span class="p">(</span><span class="s2">&quot;Equality!&quot;</span><span class="p">)</span>
<span class="linenos">23</span>        <span class="k">return</span> <span class="mi">0</span>
<span class="linenos">24</span>
<span class="linenos">25</span><span class="c1"># ----------------- end of definitions -------------------------------</span>
<span class="linenos">26</span>
<span class="linenos">27</span><span class="n">out1</span> <span class="o">=</span> <span class="n">func01_max</span><span class="p">(</span><span class="mi">5</span><span class="p">,</span> <span class="mi">55</span><span class="p">)</span>
<span class="linenos">28</span><span class="nb">print</span><span class="p">(</span><span class="s2">&quot;In first comparison, the winner is:&quot;</span><span class="p">,</span> <span class="n">out1</span><span class="p">)</span>
<span class="linenos">29</span>
<span class="linenos">30</span><span class="n">out2</span> <span class="o">=</span> <span class="n">func01_max</span><span class="p">(</span><span class="s2">&quot;a&quot;</span><span class="p">,</span> <span class="s2">&quot;A&quot;</span><span class="p">)</span>
<span class="linenos">31</span><span class="nb">print</span><span class="p">(</span><span class="s2">&quot;comparing small/large letters, the winner is:&quot;</span><span class="p">,</span> <span class="n">out2</span><span class="p">)</span>
</pre></div>
</div>
</td>
</tr>
</tbody>
</table>
<table class="docutils align-default">
<colgroup>
<col style="width: 100%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>File <strong>prog_file_01.py</strong>:</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="linenos"> 1</span><span class="c1"># Example program for importing</span>
<span class="linenos"> 2</span><span class="c1"># *yes* using __name__</span>
<span class="linenos"> 3</span>
<span class="linenos"> 4</span><span class="kn">import</span> <span class="nn">numpy</span> <span class="k">as</span> <span class="nn">np</span>
<span class="linenos"> 5</span>
<span class="linenos"> 6</span>
<span class="linenos"> 7</span><span class="k">def</span> <span class="nf">func01_max</span><span class="p">(</span><span class="n">x</span><span class="p">,</span> <span class="n">y</span><span class="p">):</span>
<span class="linenos"> 8</span>    <span class="sd">&#39;&#39;&#39;func01_max(x, y)</span>
<span class="linenos"> 9</span>
<span class="linenos">10</span><span class="sd">    This program takes two inputs and compares them to find</span>
<span class="linenos">11</span><span class="sd">    the maximum.  </span>
<span class="linenos">12</span>
<span class="linenos">13</span><span class="sd">    The maximum value is output; if they are equal, 0 is returned.&#39;&#39;&#39;</span>
<span class="linenos">14</span>    
<span class="linenos">15</span>    <span class="k">if</span> <span class="n">x</span> <span class="o">&gt;</span> <span class="n">y</span><span class="p">:</span>
<span class="linenos">16</span>        <span class="nb">print</span><span class="p">(</span><span class="s2">&quot;first value is greater&quot;</span><span class="p">)</span>
<span class="linenos">17</span>        <span class="k">return</span> <span class="n">x</span>
<span class="linenos">18</span>    <span class="k">elif</span> <span class="n">y</span> <span class="o">&gt;</span> <span class="n">x</span><span class="p">:</span>
<span class="linenos">19</span>        <span class="nb">print</span><span class="p">(</span><span class="s2">&quot;second value is greater&quot;</span><span class="p">)</span>
<span class="linenos">20</span>        <span class="k">return</span> <span class="n">y</span>
<span class="linenos">21</span>    <span class="k">else</span><span class="p">:</span>
<span class="linenos">22</span>        <span class="nb">print</span><span class="p">(</span><span class="s2">&quot;Equality!&quot;</span><span class="p">)</span>
<span class="linenos">23</span>        <span class="k">return</span> <span class="mi">0</span>
<span class="linenos">24</span>
<span class="linenos">25</span><span class="c1"># ----------------- end of definitions -------------------------------</span>
<span class="linenos">26</span>
<span class="linenos">27</span><span class="k">if</span> <span class="vm">__name__</span> <span class="o">==</span> <span class="s2">&quot;__main__&quot;</span><span class="p">:</span>
<span class="linenos">28</span>
<span class="linenos">29</span>    <span class="n">out1</span> <span class="o">=</span> <span class="n">func01_max</span><span class="p">(</span><span class="mi">5</span><span class="p">,</span> <span class="mi">55</span><span class="p">)</span>
<span class="linenos">30</span>    <span class="nb">print</span><span class="p">(</span><span class="s2">&quot;In first comparison, the winner is:&quot;</span><span class="p">,</span> <span class="n">out1</span><span class="p">)</span>
<span class="linenos">31</span>    
<span class="linenos">32</span>    <span class="n">out2</span> <span class="o">=</span> <span class="n">func01_max</span><span class="p">(</span><span class="s2">&quot;a&quot;</span><span class="p">,</span> <span class="s2">&quot;A&quot;</span><span class="p">)</span>
<span class="linenos">33</span>    <span class="nb">print</span><span class="p">(</span><span class="s2">&quot;comparing small/large letters, the winner is:&quot;</span><span class="p">,</span> <span class="n">out2</span><span class="p">)</span>
</pre></div>
</div>
</td>
</tr>
</tbody>
</table>
</div></blockquote>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<div class="sphinx-toc sphinxglobaltoc">
<h3><a href="../index.html">Table of Contents</a></h3>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="overview.html">1.  Overview</a></li>
<li class="toctree-l1"><a class="reference internal" href="notations.html">2.  Notations in these notes</a></li>
<li class="toctree-l1"><a class="reference internal" href="setup.html">3.  Python setup and environments</a></li>
<li class="toctree-l1"><a class="reference internal" href="basic_ops_types.html">4.  Basic operations and types</a></li>
<li class="toctree-l1"><a class="reference internal" href="assignment_var.html">5.  Assignment, variables and in-place operators</a></li>
<li class="toctree-l1"><a class="reference internal" href="comm_str_print.html">6.  Strings, comments and print: Help humans program</a></li>
<li class="toctree-l1"><a class="reference internal" href="indices.html">7.  Indices, slices and half-open intervals</a></li>
<li class="toctree-l1"><a class="reference internal" href="user_input.html">8.  User input</a></li>
<li class="toctree-l1"><a class="reference internal" href="modules.html">9.  Modules, I: Expand functionality</a></li>
<li class="toctree-l1"><a class="reference internal" href="helpdocs.html">10.  Functions, I: Read help docs and use</a></li>
<li class="toctree-l1"><a class="reference internal" href="boolean_expr.html">11.  Bools and Boolean expressions</a></li>
<li class="toctree-l1"><a class="reference internal" href="float_rep.html">12.  Be careful: Working with floats</a></li>
<li class="toctree-l1"><a class="reference internal" href="conditionals.html">13.  Conditionals, I: <strong>if</strong>, <strong>else</strong> and more</a></li>
<li class="toctree-l1"><a class="reference internal" href="arrays.html">14.  Arrays, I, and indexing: 1D data</a></li>
<li class="toctree-l1"><a class="reference internal" href="plot.html">15.  Plotting, I: View 1D array(s)</a></li>
<li class="toctree-l1"><a class="reference internal" href="loop_for.html">16.  Loops, I:  <strong>for</strong></a></li>
<li class="toctree-l1"><a class="reference internal" href="methods.html">17.  Methods, I</a></li>
<li class="toctree-l1"><a class="reference internal" href="complex.html">18.  Complex type, I</a></li>
<li class="toctree-l1"><a class="reference internal" href="str_and_format.html">19.  Strings, II, and formatting</a></li>
<li class="toctree-l1"><a class="reference internal" href="list_and_list_comprehension.html">20.  Lists, I: storing more stuff</a></li>
<li class="toctree-l1"><a class="reference internal" href="cp_mutable_objs.html">21.  Be careful: copying arrays, lists (and more)</a></li>
<li class="toctree-l1"><a class="reference internal" href="functions00.html">22.  Functions, IIa: Definition and arguments</a></li>
<li class="toctree-l1"><a class="reference internal" href="functions01.html">23.  Functions, IIb: Scope of variables</a></li>
<li class="toctree-l1"><a class="reference internal" href="arrays_02.html">24.  Arrays, II</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">25.  Modules, II: Make and import your own</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#modules-and-modularity">25.1.  Modules and modularity</a></li>
<li class="toctree-l2"><a class="reference internal" href="#making-a-module-in-python">25.2.  Making a module in Python</a></li>
<li class="toctree-l2"><a class="reference internal" href="#example-files-to-import">25.3.  Example files to import</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="file_io.html">26.  Reading and writing files</a></li>
<li class="toctree-l1"><a class="reference internal" href="conditionals_02.html">27.  Conditionals, II: Nesting</a></li>
<li class="toctree-l1"><a class="reference internal" href="loop_while_break_continue.html">28.  Loops, II:  <strong>while, break, continue</strong></a></li>
<li class="toctree-l1"><a class="reference internal" href="mod_data_sci.html">29.  Data science (with Pandas module)</a></li>
</ul>
</div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../_sources/pages/importing.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>document.getElementById('searchbox').style.display = "block"</script>
        </div>
      </div>
    
    
        <div class="sidebar-toggle-group no-js">
            
            <button class="sidebar-toggle" id="sidebar-hide" title="Hide the sidebar menu">
                 «
                <span class="show-for-small">hide menu</span>
                
            </button>
            <button class="sidebar-toggle" id="sidebar-show" title="Show the sidebar menu">
                
                <span class="show-for-small">menu</span>
                <span class="hide-for-small">sidebar</span>
                 »
            </button>
        </div>
    
      <div class="clearer"></div>
    </div>
    <div class="relbar-bottom">
        
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="file_io.html" title="26. Reading and writing files"
             >next</a> &nbsp; &nbsp;</li>
        <li class="right" >
          <a href="arrays_02.html" title="24. Arrays, II"
             >previous</a> &nbsp; &nbsp;</li>
    <li><a href="../index.html">AIMS Python 0.7 documentation</a> &#187;</li>

        <li class="nav-item nav-item-this"><a href=""><span class="section-number">25.  </span>Modules, II: Make and import your own</a></li> 
      </ul>
    </div>
    </div>

    <div class="footer" role="contentinfo">
        &#169; Copyright 2020, AIMS Python Consortium.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 5.0.2.
    </div>
    <!-- cloud_sptheme 1.4 -->
  </body>
</html>